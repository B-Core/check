# Check.js

Javascript input and schema checking library.

# Installation

```bash
npm install microcheck.js
```

# Usage

The module is packaged for all module style.
The main one in for ES6 import.

### ES6

For general import.

```javascript
import microcheck.js from 'micro-check.js';
```

### CommonJS

```javascript
const microcheck = require('micro-check.js');
```

### IIFE (Immediatly Invocked Function Env)

This for browser environment generally.
It will create a microcheck global variable.

```javascript
<script src="./node_modules/micro-check.js/index.iife.js"></script>
```

### UMD (Universal Module Definition)

see [ES6](#ES6)
see [CommonJS](#CommonJS)


### Simple Schema validation
```javascript
// this is the schema to check
const schema = {
  name: String, // define types you want to validate and keys name
  data: Array,
  functions: Object,
};

const schemaGood = {
  name: 'test',
  data: [],
  functions: {
    test: function() {
      return true;
    },
  },
};

const schemaBad = {
  name: 'test',
  data: {},
  functions: {},
};

const schemaBadKey = {
  name: 'test',
  data: {},
};

check.validate(schemaGood, schema)
// null

check.validate(schemaBad, schema)
// Error("data should be of type Array")

check.validate(schemaBadKey, schema)
// Error("missing keys: functions")
```

### Variable keys

```javascript
const schema = {
  name: String,
  '?data': Array,
  '?functions': Object,
};

const schemaGood = {
  name: 10,
  data: [],
};

const schemaBad = {
  name: 'test',
  data: {},
};

check.validate(schemaGood, schema)
  // null

check.validate(schemaBad, schema)
// Error("data should be of type Array")
```

### Number Constraints

```javascript

const numberSupSchema = {
  age: '>10',
};
const numberMinSchema = {
  age: '<10',
};
const numberRangeSchema = {
  age: '10<>100',
};

const numberFloatSupSchema = {
  age: '>10.15',
};
const numberFloatMinSchema = {
  age: '<10.15',
};
const numberFloatRangeSchema = {
  age: '10.4<>10.7',
};

check.validate({ age: 11 }, numberSupSchema);
// null
check.validate({ age: 11 }, numberSupSchema);
// null
check.validate({ age: -11 }, numberSupSchema);
// Error("must respect age > 10")
check.validate({ age: 10 }, numberSupSchema);
// Error("must respect age > 10")

check.validate({ age: 9 }, numberMinSchema);
// null
check.validate({ age: -9 }, numberMinSchema);
// null
check.validate({ age: 10 }, numberMinSchema);
// Error("must respect <10");

check.validate({ age: 50 }, numberRangeSchema);
// null
check.validate({ age: 11 }, numberRangeSchema);
// null
check.validate({ age: 10 }, numberRangeSchema);
// Error("must respect 10<>100")
check.validate({ age: 100 }, numberRangeSchema);
// Error("must respect 10<>100")

check.validate({ age: 10.16 }, numberFloatSupSchema);
// null
check.validate({ age: 10.16 }, numberFloatSupSchema);
// null
check.validate({ age: -11 }, numberFloatSupSchema);
// Error("must respect >10.15");
check.validate({ age: 10.14 }, numberFloatSupSchema);
// Error("must respect >10.15");

check.validate({ age: 10.14 }, numberFloatMinSchema);
// null
check.validate({ age: -9 }, numberFloatMinSchema);
// null
check.validate({ age: 10.15 }, numberFloatMinSchema);
// Error("must respect <10.15");

check.validate({ age: 10.5 }, numberFloatRangeSchema);
// null
check.validate({ age: 10.6 }, numberFloatRangeSchema);
// null
check.validate({ age: 10.2 }, numberFloatRangeSchema);
// Error("must respect 10.4<>10.7");
check.validate({ age: 10.7 }, numberFloatRangeSchema);
// Error("must respect 10.4<>10.7");
```

### Regex or Strict equality

```javascript
const schema = {
  name: new RegExp("^[a-zA-Z0-9\-_]{1,}$"),
  "?data": Object,
};

check.validate({ name: "ok" }, schema);
// null
check.validate({ name: "not ok" }, constructorSchema);
// Error("must respect {
    name: new RegExp("^[a-zA-Z0-9\-_]{1,}$"),
      "?data": Object,
}");


check.validate({ name: "test" }, { name: 'test' });
// null
check.validate({ name: "notok" }, { name: 'test' });
// TypeError;
```

### Array difference

```javascript

const arrayGood = ['test', 10, { name: 'inside_test' }, [], [10]];
// order of data is not important
//  [[], [10], 'test', 10, { name: 'inside_test' }],
//  ['test', [10], { name: 'inside_test' }, 10, []],
//  [[10], 'test', { name: 'inside_test' }, 10, []]

const arrayBad = ['test'];

check.validate({ data: arrayGood }, arraySchema);
// null

check.validate({ data: arrayBad }, arraySchema);
// Error(`must respect ${arrayGood}`)
```

### Custom functions

```javascript
const testVars = ['test', 10];
const testInput = { name: 'test' };
const schema = {
  name: input => testVars.includes(input),
};


check.validate(testInput, schema);
// null
check.validate({ name: 'test' }, schema);
// null
check.validate({ name: 'lol' }, schema)
// Error("ValidationError: "lol" did not pass input => testVars.includes(input): returned false")
```

#### License

Copyright 2018 Ciro DE CARO

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

