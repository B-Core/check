const baseConfig = {
  name: 'microcheck',
  entry: 'index.js',
  output: {
    file: 'dist/index.js',
    format: 'es',
  },
};

export default [
  baseConfig,
  {
    ...baseConfig,
    output: {
      file: 'dist/index.umd.js',
      format: 'umd',
    }
  },
  {
    ...baseConfig,
    output: {
      file: 'dist/index.iife.js',
      format: 'iife',
    }
  },
  {
    ...baseConfig,
    output: {
      file: 'dist/index.cjs.js',
      format: 'cjs',
    }
  }
];
